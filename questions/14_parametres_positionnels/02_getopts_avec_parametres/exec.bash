#! /bin/bash
# Notez la boucle While et le getopts qui est la condition de la boucle.  Donc tant qu'il y a des options, il y aura une itération.
# Option est le nom de la variable qui contiendra l'option lue pour l'itération.
#
Silence="Faux"
FichierSortie=""
while getopts 'so:' Option
do
   case $Option in
# +TODO
        ??)     ??
                ;;
                
        ??)     ??
                ;;
# -TODO
   esac
done
if [ "$FichierSortie" = "" ]; then
   echo "Le fichier de sortie est invalide"
else
   rm -f $FichierSortie
   if [ "$Silence" = "Faux" ]; then
      echo -n "Envoi de la phrase 'Bravo!' dans le fichier $FichierSortie : "
   fi
   
   echo "Bravo!" > $FichierSortie
   if [ -e $FichierSortie ]; then
      echo "Fichier créé"
   else
      echo "Fichier non créé"
   fi
fi
