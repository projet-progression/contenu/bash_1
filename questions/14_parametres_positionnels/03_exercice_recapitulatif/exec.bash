#! /bin/bash
# Notez la boucle While et le getopts qui est la condition de la boucle.  Donc tant qu'il y a des options, il y aura une itération.
# Option est le nom de la variable qui contiendra l'option lue pour l'itération.
#
Role="étudiant"
Nom="Non spécifié"
Cours="Cours inconnu"
Params="Faux"
# +TODO





# -TODO
if [ "$Params" = "Vrai" ]; then
   echo "$Nom est un $Role dans le cours $Cours ($1 $2)"
else
   echo "$Nom est un $Role dans le cours $Cours"
fi
