#! /bin/bash
#-VISIBLE
read saison
#+VISIBLE
#
# Notez que chaque bloc d'instructions se termine par deux ;
#
case $saison in
# +TODO
Été) echo "J'aime aller à la plage l'été.";;

??) echo "J'aime la couleur vert tendre des feuilles des arbres au printemps.";;

Automne) echo "J'aime le bruit des feuilles mortes quand je marche en forêt à l'automne.";;

??) echo "J'aime bien prendre un bon chocolat chaud devant le foyer après avoir pratiqué des sports d'hiver."??

??) echo "Je ne connais pas cette saison.  Est-ce une des saisons sur la planète Mars?";;
# -TODO
esac
