#! /bin/bash
LaListe=("Captain America" "Spiderman" "Superman" "Black Widow" "Cat woman" "Loki" "Thor" "Hulk" "Wolverine" "Venom")

#-VISIBLE
read Index
Index=$(( $Index - 1 ))
#+VISIBLE
# +TODO
until ??


# -TODO
# Les instructions de la boucle s'inscrivent entre les balises 'do' et 'done'
do
   # Afficher l'élément correspondant à l'index dans le tableau (vous apprendrez les notions de tableau dans le cours H43).
   if [ $Index -ge 0 ]; then
      echo -n "${LaListe[$Index]} "
   fi
   
   # Décrémenter l'index
   Index=$(( $Index - 1 ))
done
