#!/bin/bash
read UneChaine
# +TODO
# Supprimer la première partie de la chaine de caractères, jusqu'à l'adresse de courriel.
Courriel="${??#??}"

# Si la commande précédente a bien fonctionné, vous aurez l'adresse de courriel suivie d'un point-virgule et du nom d'entreprise.  Maintenant vous devez trouvez la position du point-virgule.  Indice: la fonction index de la commande expr.
PosPV=??

# Soustraire 1 à la position trouvée car vous ne désirez pas le point-virgule dans l'adresse de courriel.  Vous obtenez ainsi la longueur de l'adresse de courriel.  Cette longueur sera utile dans la prochaine étape.
PosPV=$(($PosPV - 1))

# Maintenant, en utilisant une technique du premier exercice, extraire du caractère à la position 0, pour la longueur de chaine spécifiée par la valeur dans la variable PosPV.
Courriel=??
# -TODO
echo $Courriel
