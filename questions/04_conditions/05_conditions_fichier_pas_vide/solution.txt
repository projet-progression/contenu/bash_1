# Voici les instructions que l'étudiants doit entrer pour réussir l'exercice:
if [ -e /tmp/Journal_de_bord.txt ]; then
   if [ -s /tmp/Journal_de_bord.txt ]; then
      echo "Ce fichier n'était pas vide." > /tmp/Journal_de_bord.txt
   else
      echo "Ce fichier était vide." > /tmp/Journal_de_bord.txt
   fi
else
   echo "Ce fichier n'existait pas." > /tmp/Journal_de_bord.txt
fi
