#! /bin/bash
#-VISIBLE
touch /tmp/liste_utilisateurs.txt1
touch /tmp/liste_clients.txt1
touch /tmp/liste_administrateurs.txt1
#+VISIBLE
#
# Cette fonction efface le premier fichier *.txt1 trouvé dans le dossier /tmp
# Si un fichier est trouvé, il est supprimé et la fonction retourne 0 (zéro)
# Si aucun fichier n'est trouvé, la fonction retourn 1 (un)
# 
efface_premier_fichier() {
    PremierFichier=$(ls /tmp/*.txt1 2>/dev/null | head -n 1)
    if [ "$PremierFichier" != "" ]; then
       rm -f "$PremierFichier"
       return 0
    else
       return 1
    fi
}
# +TODO
# Cette boucle efface un fichier à la fois
until [ ?? -eq 1 ];
do
   efface_premier_fichier
done
# -TODO
#-VISIBLE
if [ -e "/tmp/liste_utilisateurs.txt1" ] || [ -e "/tmp/liste_clients.txt1" ] || [ -e "/tmp/liste_administrateurs.txt1" ] ; then
   echo " "
else
   echo ""
fi
